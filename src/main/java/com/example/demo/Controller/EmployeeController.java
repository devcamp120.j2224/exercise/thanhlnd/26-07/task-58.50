package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Employee;
import com.example.demo.Respository.iEmployeeRespository;

@RestController
@CrossOrigin
public class EmployeeController {
    
    @Autowired
    iEmployeeRespository iEmployeeRespository;

    @GetMapping("employees")
    public ResponseEntity <List<Employee>> getEmployees(){
        try {
            List<Employee> listEmployees = new ArrayList<Employee>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            iEmployeeRespository.findAll().forEach(listEmployees :: add);

            if(listEmployees.size() == 0){
                return new ResponseEntity<List <Employee>>(listEmployees, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <Employee>>(listEmployees, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } 
    }
}
